#include <PID_v1.h>
#include <Adafruit_GFX.h>
#include <gfxfont.h>
#include <Adafruit_SSD1306.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define MAX_TEMP 30
#define MIN_TEMP 5

/* pinout */
// A4 - oled sda
// A5 - oled scl
// 2  - ds18b20 
// 6  - output pwm 0 - 5V
// A0 - pot
// 
extern uint8_t test[];

/* app settings */
byte pwmPin = 6;
byte sensorPin = 2;
byte control = A0;
byte relayPin = 4;
byte relaySwitch = 5;

int prevTemp = 0;
int prevUserTemp = 0;

bool useRelay = false;
double temperatureDelay = 1; //в градусах

/* display settings */
int textSize = 2;
Adafruit_SSD1306 display(4);

/* pid settings */
double userTemp, input, output;

/* тут можно увеличить скорость реагирования */
double Kp=20, Ki=0.05, Kd=0;    // постоянные
double aKp=45, aKi=0.05, aKd=0; // аггресивные параметры
double diff;

PID controller(&input, &output, &userTemp, Kp, Ki, Kd, DIRECT);

/* db18s20 */
OneWire oneWire(sensorPin);
DallasTemperature bus(&oneWire);
float oldTemp = 0, presentTemp;

enum States{
  update,
  settings,
  computePid,
  computeRelay
};
States state;

void setup() {
  bus.begin();

  Serial.begin(9600);

  state = update;

  pinMode(control, INPUT);
  pinMode(relaySwitch, INPUT);

  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, LOW);
  
  //установлена перемычка использовать реле
  useRelay = digitalRead(relaySwitch) == HIGH;

  userTemp = 18;

  controller.SetMode(AUTOMATIC);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextColor(WHITE);
  display.display();
  display.clearDisplay();
}

void loop() {
  switch(state){
    case update:
      input = readTemperature();
      //перерисовка только при изменении температуры
      if(abs(input-prevTemp) >= 1)
      {
        draw();
        prevTemp = input;
      }
      state = settings;
      break;
    case settings:
      setParams();
      state = useRelay? computeRelay : computePid;
      break;
    case computePid:
      updatePid();
      state = update;
      break;
    case computeRelay:
      updateRelay();
      state = update;
      break;
    default:
      state = update;
      break;
  }
}

void setParams(){
  userTemp = map(analogRead(control),0,1023,MAX_TEMP,MIN_TEMP);
  if(abs(prevUserTemp-userTemp) >= 1){
    draw();
    prevUserTemp = userTemp;
  }
}

void updatePid(){
  diff = abs(userTemp-input);
  if(diff < 5) {                            // Мы недалеко и используем консервативные параметры;
    controller.SetTunings(Kp, Ki, Kd);
  } else {                                // Мы далеко, используем агрессивные параметры;
    controller.SetTunings(aKp, aKi, aKd);
  }

  Serial.print("input "); Serial.println(input);
  controller.Compute();
  Serial.print("output "); Serial.println(output);
  analogWrite(pwmPin,output);
}

void updateRelay(){
  Serial.print("input_relay "); Serial.println(input);
  Serial.print("userTemp_relay "); Serial.println(userTemp);
  
  if(input <= userTemp){
     Serial.println("relay_off ");
     digitalWrite(relayPin, LOW);
  }
   
  if(input > userTemp && input - userTemp >= temperatureDelay){
    Serial.println("relay_on ");
    digitalWrite(relayPin, HIGH);
  }
}

int readTemperature(){
  int sensor = 0;
  bus.requestTemperatures();
  return bus.getTempCByIndex(sensor);
}

void draw(){
  Serial.println("draw");

  display.clearDisplay();

  display.setCursor(30,3);
  display.setTextSize(1);

  String first = "Setup=+" + String((int)userTemp)+String((char)247)+"C";
  if(userTemp < 0)
     first = "Setup=-" + String((int)userTemp)+String((char)247)+"C";

  display.println(first);
  display.setCursor(17,15);
  display.setTextSize(2);

  String second = "Tn=+" + String((int)input)+String((char)247)+"C";
  if(input < 0)
    second = "Tn=-" + String((int)input)+String((char)247)+"C";
  
  display.println(second);
  display.display();
}
